import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    data: []
  },
  getters: {
    gdata : state => {
      return state.data 
    }
  },
  mutations: {
    setData : (state, payload) => {
      state.data = payload.data.slice(0,5);
    }
  },
  actions: {
    getData : async (context) => {
      await fetch('https://ipo-cp.ru/api/v1/bootcamps/605c5f71bc557b46b4f42a56/courses')
                    .then (function (response) {
                      return response.json()
                    })
                    .then (function(json){
                      context.commit('setData', json);
                    })
                    .catch(function (error) {
                      console.log('error', error)
                    })
    }
  },
  modules: {
  }
})
